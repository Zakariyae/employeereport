package com.er.shop.services;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.er.shop.daos.EmployeeDAO;
import com.er.shop.domains.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

    private final List<Employee> employees;

    @Mock
    private EmployeeDAO dao;

    @InjectMocks
    private EmployeeService service;

    public EmployeeServiceTest() {
        employees = initEmployees();
    }

    @Test
    public void shouldReturnEmployeesOlderThan18() {
        when(dao.findAll()).thenReturn(employees);

        List<Employee> employeesOlderThan18 = service.findEmployeesOlderThan18();

        assertThat(employeesOlderThan18)
                .isNotNull()
                .isEqualTo(employees.stream().filter(employee -> employee.getAge() > 18).collect(Collectors.toList()));
    }

    @Test
    public void shouldReturnEmployeesCapitalized() {
        when(dao.findAll()).thenReturn(employees);

        List<Employee> employeesCapitalized = service.findEmployeesCapitalized();

        assertThat(employeesCapitalized)
                .isNotNull()
                .isEqualTo(employees.stream()
                        .map(employee -> new Employee(
                                        employee.getName().substring(0, 1).toUpperCase().concat(employee.getName().substring(1).toLowerCase()),
                                        employee.getAge()
                                )
                        ).collect(Collectors.toList())
                );
    }

    @Test
    public void shouldReturnEmployeesSortedByNameASC() {
        when(dao.findAll()).thenReturn(employees);

        List<Employee> employeesSortedByNameASC = service.findEmployeesSortedByNameASC();

        assertThat(employeesSortedByNameASC)
                .isNotNull()
                .isEqualTo(employees.stream().sorted(Comparator.comparing(Employee::getName)).collect(Collectors.toList()));
    }

    private List<Employee> initEmployees() {
        List<Employee> list = new ArrayList<>();

        list.add(new Employee("Max", 17));
        list.add(new Employee("Sepp", 18));
        list.add(new Employee("zak", 28));
        list.add(new Employee("ness", 27));
        list.add(new Employee("Nina", 15));
        list.add(new Employee("Mike", 51));

        return list;
    }

}
