package com.er.shop.utils;

import static org.assertj.core.api.Assertions.*;

import com.er.shop.domains.Employee;
import com.er.shop.exceptions.InvalidJsonContentException;
import com.er.shop.utils.readers.JsonReader;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class JsonReaderTest {

    private static final String EMPTY_JSON_FILE_PATH = "employees-empty.json";
    private static final String JSON_FILE_PATH = "employees-dummy.json";
    private static final String TEXT_FILE_PATH = "employees-dummy.text";

    private final List<Employee> employees;
    private final Reader<Employee> reader;

    public JsonReaderTest() {
        employees = initEmployees();
        reader = new JsonReader(new ObjectMapper());
    }

    @Test
    public void shouldLoadJsonFileContent() {
        assertThat(reader.loadFileDate(JSON_FILE_PATH))
                .isNotNull()
                .isEqualTo(employees);
    }

    @Test
    public void shouldThrowExceptionForNullOrEmptyFilePathArg() {
        assertThatThrownBy(() -> reader.loadFileDate(""))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("file path must be not empty");

        assertThatThrownBy(() -> reader.loadFileDate(null))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("file path must be not null");
    }

    @Test
    public void shoudThrowInvalidJsonContentExceptionForEmptyAndNonJsonFiles() {
        assertThatThrownBy(() -> reader.loadFileDate(TEXT_FILE_PATH))
                .isInstanceOf(InvalidJsonContentException.class)
                .hasMessage("error during reading json file content");

        assertThatThrownBy(() -> reader.loadFileDate(EMPTY_JSON_FILE_PATH))
                .isInstanceOf(InvalidJsonContentException.class)
                .hasMessage("error during reading json file content");
    }

    private List<Employee> initEmployees() {
        List<Employee> list = new ArrayList<>();

        list.add(new Employee("Max", 17));
        list.add(new Employee("Sepp", 18));
        list.add(new Employee("Nina", 15));
        list.add(new Employee("Mike", 51));

        return list;
    }

}
