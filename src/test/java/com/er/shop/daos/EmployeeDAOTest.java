package com.er.shop.daos;

import static org.assertj.core.api.Assertions.*;

import com.er.shop.domains.Employee;
import com.er.shop.domains.Page;
import com.er.shop.utils.readers.JsonReader;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDAOTest {

    private static final String FILE_PATH = "employees-dummy.json";

    private final List<Employee> employees;
    private final EmployeeDAO dao;

    public EmployeeDAOTest() {
        employees = initEmployees();
        dao = new EmployeeDAO(FILE_PATH, new JsonReader(new ObjectMapper()));
    }

    @Test
    public void shouldReturnEmployees() {
        assertThat(dao.findAll())
                .isNotNull()
                .isEqualTo(employees);
    }

    @Test
    public void shouldReturnEmployeesPaginated() {
        int size = 2;
        int pageNumber = 0;
        Page<Employee> page = new Page<>(size, pageNumber, employees.size(), employees.subList(pageNumber, size));

        assertThat(dao.findAllPaginated(pageNumber, size))
                .isNotNull()
                .isEqualTo(page);
    }

    @Test
    public void shouldThrowExceptionForInvalidSizeAndPageNumberValues() {
        assertThatThrownBy(() -> dao.findAllPaginated(0, 0))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("size must be greater than zero");

        assertThatThrownBy(() -> dao.findAllPaginated(-1, 1))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("page number must be positive");

        assertThatThrownBy(() -> dao.findAllPaginated(0, -1))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("size must be greater than zero");
    }

    private List<Employee> initEmployees() {
        List<Employee> list = new ArrayList<>();

        list.add(new Employee("Max", 17));
        list.add(new Employee("Sepp", 18));
        list.add(new Employee("Nina", 15));
        list.add(new Employee("Mike", 51));

        return list;
    }

}
