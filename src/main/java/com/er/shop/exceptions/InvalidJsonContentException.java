package com.er.shop.exceptions;

public class InvalidJsonContentException extends RuntimeException {

    public InvalidJsonContentException() {
        super();
    }

    public InvalidJsonContentException(String message) {
        super(message);
    }

    public InvalidJsonContentException(String message, Throwable cause) {
        super(message, cause);
    }

}
