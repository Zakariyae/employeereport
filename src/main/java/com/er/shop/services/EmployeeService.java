package com.er.shop.services;

import com.er.shop.daos.EmployeeDAO;
import com.er.shop.domains.Employee;
import lombok.AllArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class EmployeeService {

    private static final int MIN_AGE = 18;

    private final EmployeeDAO dao;

    /**
     * find all employees which are older than 18 years
     *
     * @return the list of employees which are older than 18 years
     */
    public List<Employee> findEmployeesOlderThan18() {
        return dao.findAll().stream()
                .filter(employee -> employee.getAge() > MIN_AGE)
                .collect(Collectors.toList());
    }

    /**
     * find all employees and capitalize their names
     *
     * @return the list of employees with names capitalized
     */
    public List<Employee> findEmployeesCapitalized() {
        return dao.findAll().stream()
                .map(employee -> new Employee(
                                employee.getName().substring(0, 1).toUpperCase().concat(employee.getName().substring(1).toLowerCase()),
                                employee.getAge()
                        )
                ).collect(Collectors.toList());
    }

    /**
     * find all employees and sort them by their names
     *
     * @return the sorted list of employees
     */
    public List<Employee> findEmployeesSortedByNameASC() {
        return dao.findAll().stream()
                .sorted(Comparator.comparing(Employee::getName))
                .collect(Collectors.toList());
    }

}
