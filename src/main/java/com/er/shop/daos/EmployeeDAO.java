package com.er.shop.daos;

import com.er.shop.domains.Employee;
import com.er.shop.domains.Page;
import com.er.shop.utils.Reader;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class EmployeeDAO {

    private final String filePath;
    private final Reader<Employee> reader;

    /**
     * find all employees
     *
     * @return the list of all employees
     */
    public List<Employee> findAll() {
        return reader.loadFileDate(filePath);
    }

    /**
     * find all employees with pagination
     *
     * @param pageNumber - the page number
     * @param size       - the size of the page
     * @return the Page of employees
     */
    public Page<Employee> findAllPaginated(int pageNumber, int size) {
        if (pageNumber < 0)
            throw new IllegalArgumentException("page number must be positive");

        if (size <= 0)
            throw new IllegalArgumentException("size must be greater than zero");

        List<Employee> employees = reader.loadFileDate(filePath);

        return new Page<>(size, pageNumber, employees.size(), employees.subList(pageNumber, size));
    }

}
