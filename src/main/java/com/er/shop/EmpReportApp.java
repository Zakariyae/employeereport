package com.er.shop;

import com.er.shop.daos.EmployeeDAO;
import com.er.shop.domains.Employee;
import com.er.shop.services.EmployeeService;
import com.er.shop.utils.Reader;
import com.er.shop.utils.readers.JsonReader;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EmpReportApp {

    private static final String FILE_PARAM = "file";

    public static void main(String[] args) {
        if (args == null || args.length == 0)
            throw new IllegalArgumentException("Invalid arguments: you have to specify json file path");

        if (FILE_PARAM.equals(args[0].replace("-", ""))) {
            // JsonReader
            Reader<Employee> reader = new JsonReader(new ObjectMapper());

            // DAO
            EmployeeDAO employeeDAO = new EmployeeDAO(args[1], reader);

            // Service
            EmployeeService service = new EmployeeService(employeeDAO);

            System.out.println("--------------------------------");

            // Employees older than 18
            System.out.println("### Employees older than 18 ###");
            service.findEmployeesOlderThan18()
                    .forEach(EmpReportApp::printEmployee);

            System.out.println("--------------------------------");

            // Employees capitalized
            System.out.println("### Employees capitalized ###");
            service.findEmployeesCapitalized()
                    .forEach(EmpReportApp::printEmployee);

            System.out.println("--------------------------------");

            // Employees sorted by name
            System.out.println("### Employees sorted by name ###");
            service.findEmployeesSortedByNameASC()
                    .forEach(EmpReportApp::printEmployee);

            System.out.println("--------------------------------");
        }
    }

    private static void printEmployee(Employee employee) {
        System.out.println("Name:".concat(employee.getName()).concat(", age: ").concat(String.valueOf(employee.getAge())));
    }

}
