package com.er.shop.utils;

import java.util.List;

public interface Reader<T> {

    /**
     * load file content
     *
     * @param filePath - the file path
     * @return the object list containing in the file
     */
    List<T> loadFileDate(String filePath);

}
