package com.er.shop.utils.readers;

import com.er.shop.domains.Employee;
import com.er.shop.exceptions.InvalidJsonContentException;
import com.er.shop.utils.Reader;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class JsonReader implements Reader<Employee> {

    private final ObjectMapper objectMapper;

    /**
     * {@inheritDoc}
     */
    public List<Employee> loadFileDate(String filePath) {
        if (filePath == null)
            throw new IllegalArgumentException("file path must be not null");

        if (filePath.isEmpty())
            throw new IllegalArgumentException("file path must be not empty");

        try {
            byte[] data = Files.readAllBytes(Paths.get(ClassLoader.getSystemResource(filePath).toURI()));

            return Arrays.asList(objectMapper.readValue(data, Employee[].class));
        } catch (Exception e) {
            throw new InvalidJsonContentException("error during reading json file content");
        }
    }

}
