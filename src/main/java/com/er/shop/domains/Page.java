package com.er.shop.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Objects;

@Getter
@AllArgsConstructor
public class Page<T> {

    private final int size;
    private final int pageNumber;
    private final int totalElements;

    private final List<T> contents;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Page<?> page = (Page<?>) o;
        return size == page.size &&
                pageNumber == page.pageNumber &&
                totalElements == page.totalElements &&
                Objects.equals(contents, page.contents);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, pageNumber, totalElements, contents);
    }

}
